# lab1

## Description
This is an education project written in C. It consists of 3 subprograms. The first one is for reading from files and writing to files. The second uses select() system call and waits for user input in console, when outputs it with a provided id. The third one is similar to the second, the only difference is that it uses poll() instead of select().

## Installation
To compile each program, simply run one of the following commands:
```sh
make task1
make task2
make task3
```

## Usage
To run the program, run one of the following commands:
```sh
./task1 :read.txt :write.txt
./task2 :someId
./task3 :someId
```
Arguments marked with ":" can be any appropriate file name (for task1) or string ID (for task2 and task3).