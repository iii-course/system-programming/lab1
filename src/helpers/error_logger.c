#include <errno.h>
#include<stdio.h>
#include <string.h>
#include "error_logger.h"

void log_error_from_errno() {
    char *message = strerror(errno);
    fprintf(stderr, "\nERROR: %s\n", message);
}

void log_error_from_message(const char *error_message) {
    fprintf(stderr, "\nERROR: %s\n", error_message);
}

void log_timeout_error(const char *id) {
    fprintf(stderr, "\nERROR: time is up for %s\n", id);
}
