#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "logger.h"
#include "error_logger.h"

void write_to_output_with_id(char *id) {
    int NUM_OF_BYTES_TO_READ = 1024;
    char buffer[NUM_OF_BYTES_TO_READ];

    int num_of_bytes_read = read(STDIN_FILENO, buffer, NUM_OF_BYTES_TO_READ);

    if (num_of_bytes_read == -1) {
        log_error_from_errno();
    }

    printf("\n%s: %s", id, buffer);
}