#pragma once

void log_error_from_errno();
void log_error_from_message(const char *error_message);
void log_timeout_error(const char *id);
