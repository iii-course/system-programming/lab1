#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include "helpers/error_logger.h"
#include "helpers/logger.h"

void check_string_id(char *id);

int main(int argc, char **argv) {
    char *string_id = argv[1];
    check_string_id(string_id);

    struct pollfd fds[1];
    fds[0].fd = STDIN_FILENO;
    fds[0].events = POLLIN;

    int WAIT_TIME_IN_MS = 5000;

    while (1) {
        int result = poll(fds, 1, WAIT_TIME_IN_MS);
        switch (result) {
            case 0:
                log_timeout_error(string_id);
                break;

            case -1: 
                log_error_from_errno();
                exit(1);
                break;
            
            default:
                if (fds[0].revents & POLLIN) { 
                    fds[0].revents = 0;
                    write_to_output_with_id(string_id);
                }
                break;
        }
    }
}


void check_string_id(char *id) {
    if (id == NULL) {
        log_error_from_message("No id provided");
        exit(1);
    }
}
