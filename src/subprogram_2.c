#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "helpers/error_logger.h"
#include "helpers/logger.h"

void check_string_id(char *id);

int main(int argc, char **argv) {
    char *string_id = argv[1];
    check_string_id(string_id);

    fd_set const_read_fd_set, test_read_fd_set;
    FD_ZERO(&const_read_fd_set);
    FD_SET(STDIN_FILENO, &const_read_fd_set);

    const int WAIT_TIME_IN_SECONDS = 5;
    struct timeval timer;

    while (1) {
        test_read_fd_set = const_read_fd_set;
        timer.tv_sec = WAIT_TIME_IN_SECONDS;
        timer.tv_usec = 0;
        int result = select(FD_SETSIZE, &test_read_fd_set, (fd_set *)NULL, (fd_set *)NULL, &timer);
        switch (result) {
            case 0:
                fprintf(stderr, "\nERROR: time is up for %s\n", string_id);
                break;

            case -1: 
                log_error_from_errno();
                exit(1);
                break;
            
            default:
                if (FD_ISSET(STDIN_FILENO, &test_read_fd_set)) { 
                    write_to_output_with_id(string_id);
                }
                break;
        }
    }
}

void check_string_id(char *id) {
    if (id == NULL) {
        log_error_from_message("No id provided");
        exit(1);
    }
}
