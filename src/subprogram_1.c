#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h> 
#include <stdbool.h>
#include <string.h>
#include "helpers/error_logger.h"

bool is_lower_case(char letter);
char to_upper_case(char letter);
void reformat_string(char *string, int string_len);
void check_file_names(const char *file_to_read, const char *file_to_write);
int open_for_reading(const char *filename);
int open_for_writing(const char *filename);
void close_fd(int fd);
ssize_t read_from_and_rewrite_with_reformatting(int read_fd, int write_fd);

int main(int argc, char **argv) {
    const char *file_to_read = argv[1];
    const char *file_to_write = argv[2];

    check_file_names(file_to_read, file_to_write);
    
    int read_fd = open_for_reading(file_to_read);
    int write_fd = open_for_writing(file_to_write);

    ssize_t total_bytes_read = read_from_and_rewrite_with_reformatting(read_fd, write_fd);

    printf("\nTotal number of bytes read: %zd\n", total_bytes_read);

    close_fd(read_fd);
    close_fd(write_fd);

    exit(0);
}

ssize_t read_from_and_rewrite_with_reformatting(int read_fd, int write_fd) {
    size_t MAX_NUM_OF_BYTES_TO_READ = 512;
    char buffer[MAX_NUM_OF_BYTES_TO_READ];
    ssize_t num_of_bytes_read  = read(read_fd, buffer, MAX_NUM_OF_BYTES_TO_READ);
    ssize_t total_bytes_read = 0;

    while (num_of_bytes_read > 0) {
        total_bytes_read += num_of_bytes_read;
        reformat_string(buffer, num_of_bytes_read);
        write(write_fd, buffer, num_of_bytes_read);
        num_of_bytes_read  = read(read_fd, buffer, MAX_NUM_OF_BYTES_TO_READ);
    }

    return total_bytes_read;
}

int open_for_reading(const char *filename) {
    int file_descriptor = open(filename, O_RDONLY, 0);

    if (file_descriptor == -1) {
        log_error_from_errno();
        exit(1);    
    }

    return file_descriptor;
}

int open_for_writing(const char *filename) {
    /*
    0644:
    - User: read & write
    - Group: read
    - Other: read
    */

    int file_descriptor = open(filename, O_WRONLY | O_TRUNC | O_APPEND | O_SYNC | O_CREAT, 0644);
    
    if (file_descriptor == -1) {
        log_error_from_errno();
        exit(1);      
    }

    return file_descriptor;
}

void close_fd(int fd) {
    if (close(fd) == -1) {
        log_error_from_errno();
        exit(1);
    }
}

void check_file_names(const char *file_to_read, const char *file_to_write) {
    if (file_to_read == NULL) {
        log_error_from_message("No file to read provided");
        exit(1);
    }

    if (file_to_write == NULL) {
        log_error_from_message("No file to write provided");
        exit(1);
    }
}

void reformat_string(char *string, int string_len) {
    for (int i = 0; i < string_len; i++) {
        if (is_lower_case(string[i])) {
            string[i] = to_upper_case(string[i]);
        }
    }
}

char to_upper_case(char letter) {
    if (is_lower_case(letter)) {
        int SHIFT_FROM_UPPER_TO_LOWER_IN_ASCII = 32;
        return letter - SHIFT_FROM_UPPER_TO_LOWER_IN_ASCII;
    }
    return letter;
}

bool is_lower_case(char letter) {
    return letter >= 97 && letter <= 122;
}
