CC=gcc
CFLAGS=-Wall -Werror -g -ggdb
SRC=src

task1: error_logger.o subprogram_1.o
	$(CC) error_logger.o subprogram_1.o $(CFLAGS) -o task1

task2: error_logger.o logger.o subprogram_2.o
	$(CC) error_logger.o logger.o subprogram_2.o $(CFLAGS) -o task2

task3: error_logger.o logger.o subprogram_3.o
	$(CC) error_logger.o logger.o subprogram_3.o $(CFLAGS) -o task3

logger.o: $(SRC)/helpers/logger.c
	$(CC) $(CFLAGS) -c $(SRC)/helpers/logger.c 

error_logger.o: $(SRC)/helpers/error_logger.c
	$(CC) $(CFLAGS) -c $(SRC)/helpers/error_logger.c

subprogram_1.o: $(SRC)/subprogram_1.c
	$(CC) $(CFLAGS) -c $(SRC)/subprogram_1.c

subprogram_2.o: $(SRC)/subprogram_2.c
	$(CC) $(CFLAGS) -c $(SRC)/subprogram_2.c

subprogram_3.o: $(SRC)/subprogram_3.c
	$(CC) -c $(SRC)/subprogram_3.c

clean:
	rm -rf task_1 task_2 task_3 *.o